#!/usr/bin/env python
import numpy as np
import time as tm
import math

import pycuda.driver as cuda
from pycuda.compiler import SourceModule
from pycuda import driver, compiler, gpuarray, tools
import pycuda.autoinit


class MatrixMultiply:

    def matrix_mul_serial(self, a_cpu, b_cpu):
        start = tm.time()
        idata = np.array(a_cpu, dtype=np.float32);
        idata1 = np.array(b_cpu, dtype=np.float32);
        # odata = np.zeros((idata.shape[0],idata.shape[0]))
        odata = np.dot(idata, idata1)
        end = tm.time() - start
        return end, odata.astype(np.float32).tolist();

    def matrix_mul_naive(self, a_cpu, b_cpu):
        idata = np.array(a_cpu, dtype=np.float32)
        idata2 = np.array(b_cpu, dtype=np.float32)
        odata = np.zeros((idata.shape[0], idata2.shape[1]), dtype=np.float32)
        start1 = tm.time()
        # device memory allocation
        idata_d = gpuarray.to_gpu(idata)
        idata2_d = gpuarray.to_gpu(idata2)
        odata_d = gpuarray.empty(odata.shape, odata.dtype)
        end1 = tm.time() - start1

        kernel_code_template = """
    __global__ void Matrix_multiply_naive(float *a, float *b, float *c,
    const unsigned int X, const unsigned int Y,const unsigned int Z)
    {


                int j = blockIdx.x*blockDim.x +threadIdx.x;
                int i = blockIdx.y*blockDim.y +threadIdx.y;
                float tmp;
                if(i < X && j < Z){            
                   tmp = 0.0f;
                   for (int k = 0; k < Y; k++)
                   tmp += a[i*Y+k]*b[k*Z+j];
                   c[i*Z+j] = tmp;
                }  





    }
    """
        kernel_code = kernel_code_template % {
            'L': idata.shape[1]  # fixed to use -32
            , 'W': idata.shape[0]
        }
        #   //if(idxlength && idy < width)
        mod = SourceModule(kernel_code)

        start = cuda.Event()
        end = cuda.Event()

        # function call
        func = mod.get_function('Matrix_multiply_naive')
        start.record()
        grid_x = (idata.shape[0] - 1) / 32 + 1
        grid_y = (idata2.shape[1] - 1) / 32 + 1

        func(idata_d, idata2_d, odata_d, np.int32(idata.shape[0]), np.int32(idata.shape[1]), np.int32(idata2.shape[1]),
             block=(32, 32, 1), grid=(grid_y, grid_x, 1))
        end.record()
        start2 = tm.time()
        odata = odata_d.get()
        end2 = tm.time() - start2
        odata = odata.tolist()
        end.synchronize()
        odata_d.gpudata.free()
        return odata, start.time_till(end) * 1e-3, start.time_till(end) * 1e-3 + end1 + end2;

    def matrix_mul_optimized1(self, a_cpu, b_cpu):
        idata = np.array(a_cpu, dtype=np.float32)

        idata2 = np.array(b_cpu, dtype=np.float32)
        odata = np.zeros((idata.shape[0], idata2.shape[1]), dtype=np.float32)

        start1 = tm.time()
        # device memory allocation
        idata_d = gpuarray.to_gpu(idata)
        idata2_d = gpuarray.to_gpu(idata2)
        odata_d = gpuarray.empty(odata.shape, odata.dtype)
        end1 = tm.time() - start1

        kernel_code_template = """
    __global__ void Matrix_multiply_optimized2(float *a, float *b, float *c,
    const unsigned int X, const unsigned int Y,const unsigned int Z)
    {
        int TILE_WIDTH = blockDim.y;

        __shared__ float ds_A[32][32];                                                //both a tiles and b tiles are shared
        __shared__ float ds_B[32][32];

        int bx = blockIdx.x;  
        int by = blockIdx.y;
        int tx = threadIdx.x;
        int ty = threadIdx.y;
        int Row = by * blockDim.y + ty;  
        int Col = bx * blockDim.x + tx;
        float Cvalue = 0;

        for(int t=0; t < ((Y-1)/TILE_WIDTH + 1); ++t){
            if(Row < X && t*TILE_WIDTH + tx < Y){
                ds_A[ty][tx] = a[Row * Y + t*TILE_WIDTH + tx];                     //load a tiles in to shared memory under boundary conditions
            }
            else{
                ds_A[ty][tx] = 0.0;
            }
            if(Col < Z && t*TILE_WIDTH + ty < Y){
                ds_B[ty][tx] = b[(t*TILE_WIDTH + ty)* Z + Col];                      //load b tiles into shared memory under boundary conditions
            }
            else{
                ds_B[ty][tx] = 0.0;
            }
            __syncthreads();

            for(int i = 0; i < TILE_WIDTH; ++i){
                Cvalue += ds_A[ty][i]*ds_B[i][tx];                                  //calculate the element of c with shared a tiles and b tiels
            }
            __syncthreads();
        }
        if(Row < X && Col < Z){
            c[Row*Z + Col] = Cvalue;
        }
    }
    """
        kernel_code = kernel_code_template % {
            'L': idata.shape[1]  # fixed to use -32
            , 'W': idata.shape[0]
        }
        #   //if(idxlength && idy < width)
        mod = SourceModule(kernel_code)

        start = cuda.Event()
        end = cuda.Event()

        # function call
        func = mod.get_function('Matrix_multiply_optimized2')
        start.record()

        grid_x = (idata.shape[0] - 1) / 32 + 1
        grid_y = (idata2.shape[1] - 1) / 32 + 1

        func(idata_d, idata2_d, odata_d, np.int32(idata.shape[0]), np.int32(idata.shape[1]), np.int32(idata2.shape[1]),
             block=(32, 32, 1), grid=(grid_y, grid_x, 1))
        end.record()
        start2 = tm.time()
        odata = odata_d.get()
        odata = odata.tolist()
        end2 = tm.time() - start2
        odata_d.gpudata.free()
        end.synchronize()

        return odata, start.time_till(end) * 1e-3, start.time_till(end) * 1e-3 + end1 + end2;

    def matrix_mul_optimized2(self, a_cpu, b_cpu):
        idata = np.array(a_cpu, dtype=np.float32)

        idata2 = np.array(b_cpu, dtype=np.float32)
        odata = np.zeros((idata.shape[0], idata2.shape[1]), dtype=np.float32)

        start1 = tm.time()
        # device memory allocation
        idata_d = gpuarray.to_gpu(idata)
        idata2_d = gpuarray.to_gpu(idata2)
        odata_d = gpuarray.empty(odata.shape, odata.dtype)
        end1 = tm.time() - start1

        kernel_code_template = """
    __global__ void Matrix_multiply_optimized3(float *a, float *b, float *c,
    const unsigned int X, const unsigned int Y,const unsigned int Z)
    {
        int TILE_WIDTH = blockDim.y;

        __shared__ float ds_A[32][32];                                                //both a tiles and b tiles are shared
        __shared__ float ds_B[32][32];

        int bx = blockIdx.x;  
        int by = blockIdx.y;
        int tx = threadIdx.x;
        int ty = threadIdx.y;
        int Row = by * blockDim.y + ty;  
        int Col = bx * blockDim.x + tx;
        float temp_ds_A = a[Row * Y + 0*TILE_WIDTH + tx]; 
        float temp_ds_B = b[(0*TILE_WIDTH + ty) * Z + Col];
        float Cvalue = 0;

        for(int t=1; t < ((Y-1)/TILE_WIDTH + 1) +1; ++t){
            ds_A[ty][tx] = temp_ds_A;
            ds_B[ty][tx] = temp_ds_B;
            __syncthreads();

            if(Row < X && t*TILE_WIDTH + tx < Y){
                temp_ds_A = a[Row * Y + t*TILE_WIDTH + tx];                     //load a tiles in to shared memory under boundary conditions
            }
            else{
                temp_ds_A = 0.0;
            }
            if(Col < Z && t*TILE_WIDTH + ty < Y){
                temp_ds_B = b[(t*TILE_WIDTH + ty)* Z + Col];                      //load b tiles into shared memory under boundary conditions
            }
            else{
                temp_ds_B = 0.0;
            }
            __syncthreads();

            for(int i = 0; i < TILE_WIDTH; ++i){
                Cvalue += ds_A[ty][i]*ds_B[i][tx];                                  //calculate the element of c with shared a tiles and b tiels
            }
            __syncthreads();
        }
        if(Row < X && Col < Z){
            c[Row*Z + Col] = Cvalue;
        }
    }
    """
        kernel_code = kernel_code_template % {
            'L': idata.shape[1]  # fixed to use -32
            , 'W': idata.shape[0]
        }
        #   //if(idxlength && idy < width)
        mod = SourceModule(kernel_code)

        start = cuda.Event()
        end = cuda.Event()

        # function call
        func = mod.get_function('Matrix_multiply_optimized3')
        start.record()
        grid_x = (idata.shape[0] - 1) / 32 + 1
        grid_y = (idata2.shape[1] - 1) / 32 + 1

        func(idata_d, idata2_d, odata_d, np.int32(idata.shape[0]), np.int32(idata.shape[1]), np.int32(idata2.shape[1]),
             block=(32, 32, 1), grid=(grid_y, grid_x, 1))
        end.record()
        start2 = tm.time()
        odata = odata_d.get()
        end2 = tm.time() - start2
        odata = odata.tolist()
        end.synchronize()
        odata_d.gpudata.free()
        return odata, start.time_till(end) * 1e-3, start.time_till(end) * 1e-3 + end1 + end2;

    def matrix_mul_optimized3(self, a_cpu, b_cpu):
        idata = np.array(a_cpu, dtype=np.float32)

        idata2 = np.array(b_cpu, dtype=np.float32)
        odata = np.zeros((idata.shape[0], idata2.shape[1]), dtype=np.float32)

        start1 = tm.time()
        # device memory allocation
        idata_d = gpuarray.to_gpu(idata)
        idata2_d = gpuarray.to_gpu(idata2)
        odata_d = gpuarray.empty(odata.shape, odata.dtype)
        end1 = tm.time() - start1

        kernel_code_template = """
    __global__ void Matrix_multiply_optimized2(float *a, float *b, float *c,
    const unsigned int X, const unsigned int Y,const unsigned int Z)
    {
           int TILE_WIDTH = blockDim.y;

        __shared__ float ds_B[32][32];                                                 //b tiles are shared. A tiles are in global memory

        int bx = blockIdx.x;  
        int by = blockIdx.y;
        int tx = threadIdx.x;
        int ty = threadIdx.y;
        int Row = by * blockDim.y + ty;  
        int Col = bx * blockDim.x + tx;
        float Cvalue = 0;

        for(int t=0; t < ((Y-1)/TILE_WIDTH + 1); ++t){
            if(Col < Z && t*TILE_WIDTH + ty <Y ){
                ds_B[ty][tx] = b[(t*TILE_WIDTH + ty)* Z + Col];                    //load b tiles into shared memory
            }
            else{
                ds_B[ty][tx] = 0.0;
            }
            __syncthreads();

            for(int i = 0; i < TILE_WIDTH; ++i){
                if(Row < X && t*TILE_WIDTH + i < Y){                               //boundary conditions
                    Cvalue += a[Row * Y + t*TILE_WIDTH + i]*ds_B[i][tx];           //calculate the element in c with shared b and global a
                }
            }
            __syncthreads();
        }
        if(Row < X && Col < Z){
            c[Row*Z + Col] = Cvalue;
        }
    }
    """
        kernel_code = kernel_code_template % {
            'L': idata.shape[1]  # fixed to use -32
            , 'W': idata.shape[0]
        }
        #   //if(idxlength && idy < width)
        mod = SourceModule(kernel_code)

        start = cuda.Event()
        end = cuda.Event()

        # function call
        func = mod.get_function('Matrix_multiply_optimized2')
        start.record()

        grid_x = (idata.shape[0] - 1) / 32 + 1
        grid_y = (idata2.shape[1] - 1) / 32 + 1

        func(idata_d, idata2_d, odata_d, np.int32(idata.shape[0]), np.int32(idata.shape[1]), np.int32(idata2.shape[1]),
             block=(32, 32, 1), grid=(grid_y, grid_x, 1))
        end.record()
        start2 = tm.time()
        odata = odata_d.get()
        odata = odata.tolist()
        end2 = tm.time() - start2
        end.synchronize()
        odata_d.gpudata.free()
        return odata, start.time_till(end) * 1e-3, start.time_till(end) * 1e-3 + end1 + end2;


if __name__ == "__main__":
    mm = MatrixMultiply();

    a_cpu = np.random.randint(10, size=(128, 10)).astype(np.float32)
    soft = SoftMax()
    b_cpu, y, time = soft.serial(a_cpu)

    d_cpu, y2, time1, time2 = soft.softmax(a_cpu)
    e_cpu, y3, time3, time4 = soft.softmaxn(a_cpu)

    print('----------------- layer {} -----------------'.format('softmax'))

    print('serial:{}ser_fc_time:{}'.format('\n', time))
    print('method1:{}{}:{}{}:{}'.format('\n',
                                        'whole_time1', time2,
                                        'kernel_time1', time1))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time1,
        time / time2))
    print('method2:{}{}:{}{}:{}'.format('\n',
                                        'whole_time2', time4,
                                        'kernel_time2', time3))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time3,
        time / time4))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time3,
        time2 / time4))

