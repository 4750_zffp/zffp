This is the finished version of ZFFP: Accelerating Forward Propagation of ZF-Net using PyCUDA



To run the program, do 'sh cnn' in this directory, or use the sbatch command.

Description of every .py file:
(1) convolution.py: the class of convolution layers
(2) fc.py: the class of fully-connected layers
(3) softmax.py: the class of softmax funtion
(4) test_funcs.py: testing functions, mainly for fully-connected layers
(5) run.py: run this file to get the result
(4) utils.py: some other funtions, such as block size assigning
