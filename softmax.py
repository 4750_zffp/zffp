#!/usr/bin/env python
import numpy as np
import time as tm
import math
from fc import MatrixMultiply
import pycuda.driver as cuda
from pycuda.compiler import SourceModule
from pycuda import driver, compiler, gpuarray, tools
import pycuda.autoinit


class SoftMax:
    def serial(self, x):
        orig_shape = x.shape
        start = tm.time()
        if len(x.shape) > 1:
            # Matrix
            exp_minmax = lambda x: np.exp(x)
            denom = lambda x: 1.0 / np.sum(x)
            x = np.apply_along_axis(exp_minmax, 1, x)
            denominator = np.apply_along_axis(denom, 1, x)

            if len(denominator.shape) == 1:
                denominator = denominator.reshape((denominator.shape[0], 1))

            x = x * denominator
            y = np.argmax(x, axis=1)
        else:
            # Vector
            x_max = np.max(x)
            x = x - x_max
            numerator = np.exp(x)
            denominator = 1.0 / np.sum(numerator)
            x = numerator.dot(denominator)
            y = argmax(x)

        assert x.shape == orig_shape
        end = tm.time()
        return x, y, end - start

    def softmax(self, a_cpu):

        idata = a_cpu.astype(np.float32)

        odata = np.zeros(a_cpu.shape, dtype=np.float32)
        odata2_d = np.zeros((1, odata.shape[0]), dtype=np.int32)
        start1 = tm.time()
        # device memory allocation
        idata_d = gpuarray.to_gpu(idata)
        odata_d = gpuarray.empty(odata.shape, odata.dtype)
        odata2_d = gpuarray.empty((1, odata.shape[0]), np.int32)

        end1 = tm.time() - start1

        kernel_code_template = """
            #include <stdio.h>


            __global__ void softmax(float* input, float* C,int* odata)
            {   
                unsigned int t = threadIdx.x;
                unsigned int start = blockIdx.x*blockDim.x;
                __shared__ float partialSum1[10];
                partialSum1[t] = input[start+t];
                __shared__ float result[10];


                /*
                if(start+t == 0){
                for(int i = 0; i < 10; i++){

                printf(\"%d \",partialSum1[i]);

                }
                  printf(\"\\n\");
                  printf(\"\\n\");
                }
                */

                __syncthreads();
                for (unsigned int stride = 1; stride <= 8; stride *= 2)
                {
                __syncthreads();
                int index = stride * threadIdx.x* 2;
                if(index<10){
                if(partialSum1[index] < partialSum1[index+stride]){
                   partialSum1[index] = partialSum1[index+stride];
                }
                }
                __syncthreads();

                }


                __syncthreads();


                t = threadIdx.x;
                start = blockIdx.x*blockDim.x;
                __shared__ float partialSum2[10];
                float tempf= expf(input[start+t]-partialSum1[0]);
                partialSum2[t] = tempf;

                /*
                if(start+t == 0){
                for(int i = 0; i < 10; i++){

                printf(\"%d \",partialSum2[i]);

                }
                  printf(\"\\n\");
                  printf(\"\\n\");
                }
                */

                __syncthreads();
                for (unsigned int stride = 1; stride <= 8; stride *= 2)
                {
                __syncthreads();
                int index = stride * threadIdx.x* 2;
                if(index<10)
                partialSum2[index]+= partialSum2[index+stride];
                __syncthreads();

                }


                __syncthreads();




                 result[t] = tempf / partialSum2[0];
                __shared__ int mins[10];
                mins[t] = t;



                for (unsigned int stride = 1; stride <= 8; stride *= 2)
                {
                __syncthreads();
                int index = stride * threadIdx.x* 2;
                if(index<10){
                if(result[mins[index]] < result[mins[index+stride]]){
                   mins[index] = mins[index+stride];
                }
                }
                __syncthreads();

                }


                __syncthreads();
                if(t<10)
                C[start+t] = result[t];
                 __syncthreads();
                if(t==0){
                odata[blockIdx.x] = mins[0];
                }


                 __syncthreads();
            }




        """

        kernel_code = kernel_code_template
        #   //if(idxlength && idy < width)
        mod = SourceModule(kernel_code)

        start = cuda.Event()
        end = cuda.Event()

        # function call
        func = mod.get_function('softmax')
        start.record()

        func(idata_d, odata_d, odata2_d, block=(10, 1, 1), grid=(idata.shape[0], 1, 1))
        end.record()
        start2 = tm.time()
        odata = odata_d.get()
        odata2 = odata2_d.get()
        end2 = tm.time() - start2

        end.synchronize()

        return odata, odata2, start.time_till(end) * 1e-3, start.time_till(end) * 1e-3 + end1 + end2;

    def softmaxn(self, a_cpu):

        idata = a_cpu.astype(np.float32)

        odata = np.zeros(a_cpu.shape, dtype=np.float32)

        start1 = tm.time()
        # device memory allocation
        idata_d = gpuarray.to_gpu(idata)
        odata_d = gpuarray.empty(odata.shape, odata.dtype)
        odata2_d = gpuarray.empty((1, odata.shape[0]), np.int32)

        end1 = tm.time() - start1

        kernel_code_template = """
            #include <stdio.h>


            __global__ void softmaxn(float* input, float* C,int* odata)
            {   
                unsigned int t = threadIdx.x;
                unsigned int start = blockIdx.x*blockDim.x;

                __shared__ float result[10];

               float maxv = input[start];
               int maxi =0;

               for(int i = 0; i<9;i++){
                  if(maxv<input[start+i+1]){
                  maxv = input[start+i+1];
                  maxi = i+1;
                  }
               }

                if(t==0){
                odata[blockIdx.x] = maxi;
                } 


                __syncthreads();


                t = threadIdx.x;
                start = blockIdx.x*blockDim.x;
                __shared__ float partialSum2[10];
                float tempf= expf(input[start+t]-maxv);
                partialSum2[t] = tempf;

                /*
                if(start+t == 0){
                for(int i = 0; i < 10; i++){

                printf(\"%d \",partialSum2[i]);

                }
                  printf(\"\\n\");
                  printf(\"\\n\");
                }
                */

                __syncthreads();
                for (unsigned int stride = 1; stride <= 8; stride *= 2)
                {
                __syncthreads();
                int index = stride * threadIdx.x* 2;
                if(index<10)
                partialSum2[index]+= partialSum2[index+stride];
                __syncthreads();

                }


                __syncthreads();




                 result[t] = tempf / partialSum2[0];
                __shared__ int mins[10];
                mins[t] = t;
                 __syncthreads();


                for (unsigned int stride = 1; stride <= 8; stride *= 2)
                {
                __syncthreads();
                int index = stride * threadIdx.x* 2;
                if(index<10){
                if(result[mins[index]] < result[mins[index+stride]]){
                   mins[index] = mins[index+stride];
                }
                }
                __syncthreads();

                }

                __syncthreads();
                if(t<10)
                C[start+t] = result[t];




                 __syncthreads();
            }




        """

        kernel_code = kernel_code_template
        #   //if(idxlength && idy < width)
        mod = SourceModule(kernel_code)

        start = cuda.Event()
        end = cuda.Event()

        # function call
        func = mod.get_function('softmaxn')
        start.record()

        func(idata_d, odata_d, odata2_d, block=(10, 1, 1), grid=(idata.shape[0], 1, 1))
        end.record()
        start2 = tm.time()
        odata = odata_d.get()
        odata2 = odata2_d.get()
        end2 = tm.time() - start2

        end.synchronize()

        return odata, odata2, start.time_till(end) * 1e-3, start.time_till(end) * 1e-3 + end1 + end2;


def testsoftmax():
    a_cpu = np.random.randn(128, 10).astype(np.float32)
    soft = SoftMax()
    b_cpu, y, time = soft.serial(a_cpu)

    d_cpu, y2, time1, time2 = soft.softmax(a_cpu)

    e_cpu, y3, time3, time4 = soft.softmaxn(a_cpu)

    print('----------------- layer {} -----------------'.format('softmax'))

    print('serial:{}ser_fc_time:{}'.format('\n', time))
    print('method1:{}{}:{}{}:{}'.format('\n',
                                        'whole_time1', time2,
                                        'kernel_time1', time1))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time1,
        time / time2))
    print('method2:{}{}:{}{}:{}'.format('\n',
                                        'whole_time2', time4,
                                        'kernel_time2', time3))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time3,
        time / time4))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time3,
        time2 / time4))


def testfc():
    mm = MatrixMultiply();

    a_cpu = np.random.randn(128, 9216).astype(np.float32)
    b_cpu = np.random.randn(9216, 4096).astype(np.float32)

    a_cpu = a_cpu.tolist()
    b_cpu = b_cpu.tolist()

    print('----------------- layer {} -----------------'.format('fc1'))
    time, m_cpu = mm.matrix_mul_serial(a_cpu, b_cpu)
    c_cpu, time1, time01 = mm.matrix_mul_naive(a_cpu, b_cpu)
    d_cpu, time2, time02 = mm.matrix_mul_optimized1(a_cpu, b_cpu)
    e_cpu, time3, time03 = mm.matrix_mul_optimized2(a_cpu, b_cpu)
    f_cpu, time4, time04 = mm.matrix_mul_optimized3(a_cpu, b_cpu)
    print('serial:{}ser_fc_time:{}'.format('\n', time))
    print('method1:{}{}:{}{}:{}'.format('\n',
                                        'whole_time1', time01,
                                        'kernel_time1', time1))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time1,
        time / time01))
    print('method2:{}{}:{}{}:{}'.format('\n',
                                        'whole_time2', time04,
                                        'kernel_time2', time4))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time4,
        time / time04))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time4,
        time01 / time04))
    print('method3:{}{}:{}{}:{} '.format('\n',
                                         'whole_time3', time02,
                                         'kernel_time3', time2))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time2,
        time / time02))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time2,
        time01 / time02))
    print('method4:{}{}:{}{}:{} '.format('\n',
                                         'whole_time4', time03,
                                         'kernel_time4', time3))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time3,
        time / time03))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time3,
        time01 / time03))

    a_cpu = np.random.randn(128, 4096).astype(np.float32)
    b_cpu = np.random.randn(4096, 4096).astype(np.float32)

    a_cpu = a_cpu.tolist()
    b_cpu = b_cpu.tolist()

    time, m_cpu = mm.matrix_mul_serial(a_cpu, b_cpu)
    c_cpu, time1, time01 = mm.matrix_mul_naive(a_cpu, b_cpu)
    d_cpu, time2, time02 = mm.matrix_mul_optimized1(a_cpu, b_cpu)
    e_cpu, time3, time03 = mm.matrix_mul_optimized2(a_cpu, b_cpu)
    f_cpu, time4, time04 = mm.matrix_mul_optimized3(a_cpu, b_cpu)
    print('----------------- layer {} -----------------'.format('fc2'))
    time, m_cpu = mm.matrix_mul_serial(a_cpu, b_cpu)
    c_cpu, time1, time01 = mm.matrix_mul_naive(a_cpu, b_cpu)
    d_cpu, time2, time02 = mm.matrix_mul_optimized1(a_cpu, b_cpu)
    e_cpu, time3, time03 = mm.matrix_mul_optimized2(a_cpu, b_cpu)
    f_cpu, time4, time04 = mm.matrix_mul_optimized3(a_cpu, b_cpu)
    print('serial:{}ser_fc_time:{}'.format('\n', time))
    print('method1:{}{}:{}{}:{}'.format('\n',
                                        'whole_time1', time01,
                                        'kernel_time1', time1))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time1,
        time / time01))
    print('method2:{}{}:{}{}:{}'.format('\n',
                                        'whole_time2', time04,
                                        'kernel_time2', time4))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time4,
        time / time04))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time4,
        time01 / time04))
    print('method3:{}{}:{}{}:{} '.format('\n',
                                         'whole_time3', time02,
                                         'kernel_time3', time2))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time2,
        time / time02))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time2,
        time01 / time02))
    print('method4:{}{}:{}{}:{} '.format('\n',
                                         'whole_time4', time03,
                                         'kernel_time4', time3))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time3,
        time / time03))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time3,
        time01 / time03))

    a_cpu = np.random.randn(128, 4096).astype(np.float32)
    b_cpu = np.random.randn(4096, 10).astype(np.float32)

    a_cpu = a_cpu.tolist()
    b_cpu = b_cpu.tolist()
    time, m_cpu = mm.matrix_mul_serial(a_cpu, b_cpu)
    c_cpu, time1, time01 = mm.matrix_mul_naive(a_cpu, b_cpu)
    d_cpu, time2, time02 = mm.matrix_mul_optimized1(a_cpu, b_cpu)
    e_cpu, time3, time03 = mm.matrix_mul_optimized2(a_cpu, b_cpu)
    f_cpu, time4, time04 = mm.matrix_mul_optimized3(a_cpu, b_cpu)

    print('----------------- layer {} -----------------'.format('final fc'))
    time, m_cpu = mm.matrix_mul_serial(a_cpu, b_cpu)
    c_cpu, time1, time01 = mm.matrix_mul_naive(a_cpu, b_cpu)
    d_cpu, time2, time02 = mm.matrix_mul_optimized1(a_cpu, b_cpu)
    e_cpu, time3, time03 = mm.matrix_mul_optimized2(a_cpu, b_cpu)
    f_cpu, time4, time04 = mm.matrix_mul_optimized3(a_cpu, b_cpu)
    print('serial:{}ser_fc_time:{}'.format('\n', time))
    print('method1:{}{}:{}{}:{}'.format('\n',
                                        'whole_time1', time01,
                                        'kernel_time1', time1))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time1,
        time / time01))
    print('method2:{}{}:{}{}:{}'.format('\n',
                                        'whole_time2', time04,
                                        'kernel_time2', time4))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time4,
        time / time04))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time4,
        time01 / time04))
    print('method3:{}{}:{}{}:{} '.format('\n',
                                         'whole_time3', time02,
                                         'kernel_time3', time2))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time2,
        time / time02))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time2,
        time01 / time02))
    print('method4:{}{}:{}{}:{} '.format('\n',
                                         'whole_time4', time03,
                                         'kernel_time4', time3))
    print('speed up to serial: kernel:{} whole:{}'.format(
        time / time3,
        time / time03))
    print('speed up to naive parallel: kernel:{} whole:{}'.format(
        time1 / time3,
        time01 / time03))


if __name__ == "__main__":
    mm = MatrixMultiply();

    a_cpu = np.random.randint(10, size=(12800, 10)).astype(np.float32)
    # a_cpu = np.array([1,2,3,4,5,3,4,5,6,7])
    soft = SoftMax()
    b_cpu, y, time = soft.serial(a_cpu)
    # b_cpu, time1,time2 = soft.sum(a_cpu)

    # c_cpu, time1,time2 = soft.max(a_cpu)

    d_cpu, y2, time1, time2 = soft.softmax(a_cpu)
    e_cpu, y3, time3, time4 = soft.softmaxn(a_cpu)
